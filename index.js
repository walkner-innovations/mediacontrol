// Setup basic express server
var express = require("express");
var app = express();
var path = require("path");
var server = require("http").createServer(app);
var io = require("socket.io")(server);

// GPIO Setup
const Gpio = require('onoff').Gpio;
const led = new Gpio(17, 'out');

// Server setup
var port = process.env.PORT || 3000;
server.listen(port, () => {
  console.log("Server listening at port %d", port);
});
app.use(express.static(path.join(__dirname, "public")));

// SocketIO Bindings
io.on("connection", (socket) => {
  socket.on("value changed", (data) => {
    socket.broadcast.emit("update value", {
      who: data.who,
      value: data.value,
    });
  });
  socket.on("cvalue changed", (data) => {

    socket.broadcast.emit("update cvalue", {
      who: data.who,
      value: data.value,
    });

    console.log("LED:");
    console.log(data.value);
    led.writeSync(data.value ? 1 : 0);

  });
});
