$(function () {
  var socket = io();

  var $window = $(window);

  const sendUpdate = (signal, control, value) => {
    console.log("Send Update");
    socket.emit(signal, control, value);
  };

  // Events

  $("input.sync").on("input", function (event) {
    console.log("Value changed");
    console.log($(this).attr("id"));
    console.log($(this).val());

    socket.emit("value changed", {
      who: $(this).attr("id"),
      value: $(this).val(),
    });
    return false;
  });
  $("input.csync").on("input", function (event) {
    console.log("CValue changed");
    console.log($(this).attr("id"));
    console.log($(this).prop('checked'));

    socket.emit("cvalue changed", {
      who: $(this).attr("id"),
      value: $(this).prop('checked')
    });
    return false;
  });

  socket.on("update value", function (data) {
    $("input#" + data.who).val(data.value);

    console.log("Update Value");
    console.log(data.who);
    console.log(data.value);
  });
  socket.on("update cvalue", function (data) {
    $("input#" + data.who).prop('checked', data.value);

    console.log("Update CValue");
    console.log(data.who);
    console.log(data.value);
  });


  // Connection Indicators

  socket.on("connect", function () {
    console.log("Connected");
  });

  socket.on("disconnect", () => {
    console.log("Disconnected");
  });

  socket.on("reconnect", () => {
    console.log("Reconnected");
  });

  socket.on("reconnect_error", () => {
    console.log("Connection Error");
  });
});
